package com.fogacoop.fogacoopproject;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class Agregado implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String idSubgrupo;
	private java.lang.String idAgregado;
	private java.lang.String idSig;

	public Agregado() {
	}

	public java.lang.String getIdSubgrupo() {
		return this.idSubgrupo;
	}

	public void setIdSubgrupo(java.lang.String idSubgrupo) {
		this.idSubgrupo = idSubgrupo;
	}

	public java.lang.String getIdAgregado() {
		return this.idAgregado;
	}

	public void setIdAgregado(java.lang.String idAgregado) {
		this.idAgregado = idAgregado;
	}

	public java.lang.String getIdSig() {
		return this.idSig;
	}

	public void setIdSig(java.lang.String idSig) {
		this.idSig = idSig;
	}

	public Agregado(java.lang.String idSubgrupo, java.lang.String idAgregado,
			java.lang.String idSig) {
		this.idSubgrupo = idSubgrupo;
		this.idAgregado = idAgregado;
		this.idSig = idSig;
	}

}
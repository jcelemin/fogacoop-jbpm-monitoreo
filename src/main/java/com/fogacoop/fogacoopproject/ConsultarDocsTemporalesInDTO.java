package com.fogacoop.fogacoopproject;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class ConsultarDocsTemporalesInDTO implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String idSeguimiento;
	private java.lang.String idVisita;
	private java.lang.String idEscalar;
	private java.lang.String idVideoconferencia;
	private java.lang.String idDocumentoProceso;

	public ConsultarDocsTemporalesInDTO() {
	}

	public java.lang.String getIdSeguimiento() {
		return this.idSeguimiento;
	}

	public void setIdSeguimiento(java.lang.String idSeguimiento) {
		this.idSeguimiento = idSeguimiento;
	}

	public java.lang.String getIdVisita() {
		return this.idVisita;
	}

	public void setIdVisita(java.lang.String idVisita) {
		this.idVisita = idVisita;
	}

	public java.lang.String getIdEscalar() {
		return this.idEscalar;
	}

	public void setIdEscalar(java.lang.String idEscalar) {
		this.idEscalar = idEscalar;
	}

	public java.lang.String getIdVideoconferencia() {
		return this.idVideoconferencia;
	}

	public void setIdVideoconferencia(java.lang.String idVideoconferencia) {
		this.idVideoconferencia = idVideoconferencia;
	}

	public java.lang.String getIdDocumentoProceso() {
		return this.idDocumentoProceso;
	}

	public void setIdDocumentoProceso(java.lang.String idDocumentoProceso) {
		this.idDocumentoProceso = idDocumentoProceso;
	}

	public ConsultarDocsTemporalesInDTO(java.lang.String idSeguimiento,
			java.lang.String idVisita, java.lang.String idEscalar,
			java.lang.String idVideoconferencia,
			java.lang.String idDocumentoProceso) {
		this.idSeguimiento = idSeguimiento;
		this.idVisita = idVisita;
		this.idEscalar = idEscalar;
		this.idVideoconferencia = idVideoconferencia;
		this.idDocumentoProceso = idDocumentoProceso;
	}

}
package com.fogacoop.fogacoopproject;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class FichaSegControlesLey implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String numAhorradoresAsociadosCorteUno;
	private java.lang.String numAhorradoresAsociadosCorteDos;
	private java.lang.String numAhorradoresAsociadosCorteTres;
	private java.lang.String numAhorradoresAsociadosCorteCuatro;
	private java.lang.String montoDepositosCorteUno;
	private java.lang.String montoDepositosCorteDos;
	private java.lang.String montoDepositosCorteTres;
	private java.lang.String montoDepositosCorteCuatro;
	private java.lang.String cartera10NoAdmisibleCorteUno;
	private java.lang.String cartera10NoAdmisibleCorteDos;
	private java.lang.String cartera10NoAdmisibleCorteTres;
	private java.lang.String cartera10NoAdmisibleCorteCuatro;
	private java.lang.String cap25PatrimonioTecnicoCorteUno;
	private java.lang.String cap25PatrimonioTecnicoCorteDos;
	private java.lang.String cap25PatrimonioTecnicoCorteTres;
	private java.lang.String cap25PatrimonioTecnicoCorteCuatro;
	private java.lang.String inversionesCorteUno;
	private java.lang.String inversionesCorteDos;
	private java.lang.String inversionesCorteTres;
	private java.lang.String inversionesCorteCuatro;
	private java.lang.String numCreditosMaxLegalCorteUno;
	private java.lang.String numCreditosMaxLegalCorteDos;
	private java.lang.String numCreditosMaxLegalCorteTres;
	private java.lang.String numCreditosMaxLegalCorteCuatro;
	private java.lang.String capitalSocialCorteUno;
	private java.lang.String capitalSocialCorteDos;
	private java.lang.String capitalSocialCorteTres;
	private java.lang.String capitalSocialCorteCuatro;
	private java.lang.String capitalMinRequeridoCorteUno;
	private java.lang.String capitalMinRequeridoCorteDos;
	private java.lang.String capitalMinRequeridoCorteTres;
	private java.lang.String capitalMinRequeridoCorteCuatro;

	public FichaSegControlesLey() {
	}

	public java.lang.String getNumAhorradoresAsociadosCorteUno() {
		return this.numAhorradoresAsociadosCorteUno;
	}

	public void setNumAhorradoresAsociadosCorteUno(
			java.lang.String numAhorradoresAsociadosCorteUno) {
		this.numAhorradoresAsociadosCorteUno = numAhorradoresAsociadosCorteUno;
	}

	public java.lang.String getNumAhorradoresAsociadosCorteDos() {
		return this.numAhorradoresAsociadosCorteDos;
	}

	public void setNumAhorradoresAsociadosCorteDos(
			java.lang.String numAhorradoresAsociadosCorteDos) {
		this.numAhorradoresAsociadosCorteDos = numAhorradoresAsociadosCorteDos;
	}

	public java.lang.String getNumAhorradoresAsociadosCorteTres() {
		return this.numAhorradoresAsociadosCorteTres;
	}

	public void setNumAhorradoresAsociadosCorteTres(
			java.lang.String numAhorradoresAsociadosCorteTres) {
		this.numAhorradoresAsociadosCorteTres = numAhorradoresAsociadosCorteTres;
	}

	public java.lang.String getNumAhorradoresAsociadosCorteCuatro() {
		return this.numAhorradoresAsociadosCorteCuatro;
	}

	public void setNumAhorradoresAsociadosCorteCuatro(
			java.lang.String numAhorradoresAsociadosCorteCuatro) {
		this.numAhorradoresAsociadosCorteCuatro = numAhorradoresAsociadosCorteCuatro;
	}

	public java.lang.String getMontoDepositosCorteUno() {
		return this.montoDepositosCorteUno;
	}

	public void setMontoDepositosCorteUno(
			java.lang.String montoDepositosCorteUno) {
		this.montoDepositosCorteUno = montoDepositosCorteUno;
	}

	public java.lang.String getMontoDepositosCorteDos() {
		return this.montoDepositosCorteDos;
	}

	public void setMontoDepositosCorteDos(
			java.lang.String montoDepositosCorteDos) {
		this.montoDepositosCorteDos = montoDepositosCorteDos;
	}

	public java.lang.String getMontoDepositosCorteTres() {
		return this.montoDepositosCorteTres;
	}

	public void setMontoDepositosCorteTres(
			java.lang.String montoDepositosCorteTres) {
		this.montoDepositosCorteTres = montoDepositosCorteTres;
	}

	public java.lang.String getMontoDepositosCorteCuatro() {
		return this.montoDepositosCorteCuatro;
	}

	public void setMontoDepositosCorteCuatro(
			java.lang.String montoDepositosCorteCuatro) {
		this.montoDepositosCorteCuatro = montoDepositosCorteCuatro;
	}

	public java.lang.String getCartera10NoAdmisibleCorteUno() {
		return this.cartera10NoAdmisibleCorteUno;
	}

	public void setCartera10NoAdmisibleCorteUno(
			java.lang.String cartera10NoAdmisibleCorteUno) {
		this.cartera10NoAdmisibleCorteUno = cartera10NoAdmisibleCorteUno;
	}

	public java.lang.String getCartera10NoAdmisibleCorteDos() {
		return this.cartera10NoAdmisibleCorteDos;
	}

	public void setCartera10NoAdmisibleCorteDos(
			java.lang.String cartera10NoAdmisibleCorteDos) {
		this.cartera10NoAdmisibleCorteDos = cartera10NoAdmisibleCorteDos;
	}

	public java.lang.String getCartera10NoAdmisibleCorteTres() {
		return this.cartera10NoAdmisibleCorteTres;
	}

	public void setCartera10NoAdmisibleCorteTres(
			java.lang.String cartera10NoAdmisibleCorteTres) {
		this.cartera10NoAdmisibleCorteTres = cartera10NoAdmisibleCorteTres;
	}

	public java.lang.String getCartera10NoAdmisibleCorteCuatro() {
		return this.cartera10NoAdmisibleCorteCuatro;
	}

	public void setCartera10NoAdmisibleCorteCuatro(
			java.lang.String cartera10NoAdmisibleCorteCuatro) {
		this.cartera10NoAdmisibleCorteCuatro = cartera10NoAdmisibleCorteCuatro;
	}

	public java.lang.String getCap25PatrimonioTecnicoCorteUno() {
		return this.cap25PatrimonioTecnicoCorteUno;
	}

	public void setCap25PatrimonioTecnicoCorteUno(
			java.lang.String cap25PatrimonioTecnicoCorteUno) {
		this.cap25PatrimonioTecnicoCorteUno = cap25PatrimonioTecnicoCorteUno;
	}

	public java.lang.String getCap25PatrimonioTecnicoCorteDos() {
		return this.cap25PatrimonioTecnicoCorteDos;
	}

	public void setCap25PatrimonioTecnicoCorteDos(
			java.lang.String cap25PatrimonioTecnicoCorteDos) {
		this.cap25PatrimonioTecnicoCorteDos = cap25PatrimonioTecnicoCorteDos;
	}

	public java.lang.String getCap25PatrimonioTecnicoCorteTres() {
		return this.cap25PatrimonioTecnicoCorteTres;
	}

	public void setCap25PatrimonioTecnicoCorteTres(
			java.lang.String cap25PatrimonioTecnicoCorteTres) {
		this.cap25PatrimonioTecnicoCorteTres = cap25PatrimonioTecnicoCorteTres;
	}

	public java.lang.String getCap25PatrimonioTecnicoCorteCuatro() {
		return this.cap25PatrimonioTecnicoCorteCuatro;
	}

	public void setCap25PatrimonioTecnicoCorteCuatro(
			java.lang.String cap25PatrimonioTecnicoCorteCuatro) {
		this.cap25PatrimonioTecnicoCorteCuatro = cap25PatrimonioTecnicoCorteCuatro;
	}

	public java.lang.String getInversionesCorteUno() {
		return this.inversionesCorteUno;
	}

	public void setInversionesCorteUno(java.lang.String inversionesCorteUno) {
		this.inversionesCorteUno = inversionesCorteUno;
	}

	public java.lang.String getInversionesCorteDos() {
		return this.inversionesCorteDos;
	}

	public void setInversionesCorteDos(java.lang.String inversionesCorteDos) {
		this.inversionesCorteDos = inversionesCorteDos;
	}

	public java.lang.String getInversionesCorteTres() {
		return this.inversionesCorteTres;
	}

	public void setInversionesCorteTres(java.lang.String inversionesCorteTres) {
		this.inversionesCorteTres = inversionesCorteTres;
	}

	public java.lang.String getInversionesCorteCuatro() {
		return this.inversionesCorteCuatro;
	}

	public void setInversionesCorteCuatro(
			java.lang.String inversionesCorteCuatro) {
		this.inversionesCorteCuatro = inversionesCorteCuatro;
	}

	public java.lang.String getNumCreditosMaxLegalCorteUno() {
		return this.numCreditosMaxLegalCorteUno;
	}

	public void setNumCreditosMaxLegalCorteUno(
			java.lang.String numCreditosMaxLegalCorteUno) {
		this.numCreditosMaxLegalCorteUno = numCreditosMaxLegalCorteUno;
	}

	public java.lang.String getNumCreditosMaxLegalCorteDos() {
		return this.numCreditosMaxLegalCorteDos;
	}

	public void setNumCreditosMaxLegalCorteDos(
			java.lang.String numCreditosMaxLegalCorteDos) {
		this.numCreditosMaxLegalCorteDos = numCreditosMaxLegalCorteDos;
	}

	public java.lang.String getNumCreditosMaxLegalCorteTres() {
		return this.numCreditosMaxLegalCorteTres;
	}

	public void setNumCreditosMaxLegalCorteTres(
			java.lang.String numCreditosMaxLegalCorteTres) {
		this.numCreditosMaxLegalCorteTres = numCreditosMaxLegalCorteTres;
	}

	public java.lang.String getNumCreditosMaxLegalCorteCuatro() {
		return this.numCreditosMaxLegalCorteCuatro;
	}

	public void setNumCreditosMaxLegalCorteCuatro(
			java.lang.String numCreditosMaxLegalCorteCuatro) {
		this.numCreditosMaxLegalCorteCuatro = numCreditosMaxLegalCorteCuatro;
	}

	public java.lang.String getCapitalSocialCorteUno() {
		return this.capitalSocialCorteUno;
	}

	public void setCapitalSocialCorteUno(java.lang.String capitalSocialCorteUno) {
		this.capitalSocialCorteUno = capitalSocialCorteUno;
	}

	public java.lang.String getCapitalSocialCorteDos() {
		return this.capitalSocialCorteDos;
	}

	public void setCapitalSocialCorteDos(java.lang.String capitalSocialCorteDos) {
		this.capitalSocialCorteDos = capitalSocialCorteDos;
	}

	public java.lang.String getCapitalSocialCorteTres() {
		return this.capitalSocialCorteTres;
	}

	public void setCapitalSocialCorteTres(
			java.lang.String capitalSocialCorteTres) {
		this.capitalSocialCorteTres = capitalSocialCorteTres;
	}

	public java.lang.String getCapitalSocialCorteCuatro() {
		return this.capitalSocialCorteCuatro;
	}

	public void setCapitalSocialCorteCuatro(
			java.lang.String capitalSocialCorteCuatro) {
		this.capitalSocialCorteCuatro = capitalSocialCorteCuatro;
	}

	public java.lang.String getCapitalMinRequeridoCorteUno() {
		return this.capitalMinRequeridoCorteUno;
	}

	public void setCapitalMinRequeridoCorteUno(
			java.lang.String capitalMinRequeridoCorteUno) {
		this.capitalMinRequeridoCorteUno = capitalMinRequeridoCorteUno;
	}

	public java.lang.String getCapitalMinRequeridoCorteDos() {
		return this.capitalMinRequeridoCorteDos;
	}

	public void setCapitalMinRequeridoCorteDos(
			java.lang.String capitalMinRequeridoCorteDos) {
		this.capitalMinRequeridoCorteDos = capitalMinRequeridoCorteDos;
	}

	public java.lang.String getCapitalMinRequeridoCorteTres() {
		return this.capitalMinRequeridoCorteTres;
	}

	public void setCapitalMinRequeridoCorteTres(
			java.lang.String capitalMinRequeridoCorteTres) {
		this.capitalMinRequeridoCorteTres = capitalMinRequeridoCorteTres;
	}

	public java.lang.String getCapitalMinRequeridoCorteCuatro() {
		return this.capitalMinRequeridoCorteCuatro;
	}

	public void setCapitalMinRequeridoCorteCuatro(
			java.lang.String capitalMinRequeridoCorteCuatro) {
		this.capitalMinRequeridoCorteCuatro = capitalMinRequeridoCorteCuatro;
	}

	public FichaSegControlesLey(
			java.lang.String numAhorradoresAsociadosCorteUno,
			java.lang.String numAhorradoresAsociadosCorteDos,
			java.lang.String numAhorradoresAsociadosCorteTres,
			java.lang.String numAhorradoresAsociadosCorteCuatro,
			java.lang.String montoDepositosCorteUno,
			java.lang.String montoDepositosCorteDos,
			java.lang.String montoDepositosCorteTres,
			java.lang.String montoDepositosCorteCuatro,
			java.lang.String cartera10NoAdmisibleCorteUno,
			java.lang.String cartera10NoAdmisibleCorteDos,
			java.lang.String cartera10NoAdmisibleCorteTres,
			java.lang.String cartera10NoAdmisibleCorteCuatro,
			java.lang.String cap25PatrimonioTecnicoCorteUno,
			java.lang.String cap25PatrimonioTecnicoCorteDos,
			java.lang.String cap25PatrimonioTecnicoCorteTres,
			java.lang.String cap25PatrimonioTecnicoCorteCuatro,
			java.lang.String inversionesCorteUno,
			java.lang.String inversionesCorteDos,
			java.lang.String inversionesCorteTres,
			java.lang.String inversionesCorteCuatro,
			java.lang.String numCreditosMaxLegalCorteUno,
			java.lang.String numCreditosMaxLegalCorteDos,
			java.lang.String numCreditosMaxLegalCorteTres,
			java.lang.String numCreditosMaxLegalCorteCuatro,
			java.lang.String capitalSocialCorteUno,
			java.lang.String capitalSocialCorteDos,
			java.lang.String capitalSocialCorteTres,
			java.lang.String capitalSocialCorteCuatro,
			java.lang.String capitalMinRequeridoCorteUno,
			java.lang.String capitalMinRequeridoCorteDos,
			java.lang.String capitalMinRequeridoCorteTres,
			java.lang.String capitalMinRequeridoCorteCuatro) {
		this.numAhorradoresAsociadosCorteUno = numAhorradoresAsociadosCorteUno;
		this.numAhorradoresAsociadosCorteDos = numAhorradoresAsociadosCorteDos;
		this.numAhorradoresAsociadosCorteTres = numAhorradoresAsociadosCorteTres;
		this.numAhorradoresAsociadosCorteCuatro = numAhorradoresAsociadosCorteCuatro;
		this.montoDepositosCorteUno = montoDepositosCorteUno;
		this.montoDepositosCorteDos = montoDepositosCorteDos;
		this.montoDepositosCorteTres = montoDepositosCorteTres;
		this.montoDepositosCorteCuatro = montoDepositosCorteCuatro;
		this.cartera10NoAdmisibleCorteUno = cartera10NoAdmisibleCorteUno;
		this.cartera10NoAdmisibleCorteDos = cartera10NoAdmisibleCorteDos;
		this.cartera10NoAdmisibleCorteTres = cartera10NoAdmisibleCorteTres;
		this.cartera10NoAdmisibleCorteCuatro = cartera10NoAdmisibleCorteCuatro;
		this.cap25PatrimonioTecnicoCorteUno = cap25PatrimonioTecnicoCorteUno;
		this.cap25PatrimonioTecnicoCorteDos = cap25PatrimonioTecnicoCorteDos;
		this.cap25PatrimonioTecnicoCorteTres = cap25PatrimonioTecnicoCorteTres;
		this.cap25PatrimonioTecnicoCorteCuatro = cap25PatrimonioTecnicoCorteCuatro;
		this.inversionesCorteUno = inversionesCorteUno;
		this.inversionesCorteDos = inversionesCorteDos;
		this.inversionesCorteTres = inversionesCorteTres;
		this.inversionesCorteCuatro = inversionesCorteCuatro;
		this.numCreditosMaxLegalCorteUno = numCreditosMaxLegalCorteUno;
		this.numCreditosMaxLegalCorteDos = numCreditosMaxLegalCorteDos;
		this.numCreditosMaxLegalCorteTres = numCreditosMaxLegalCorteTres;
		this.numCreditosMaxLegalCorteCuatro = numCreditosMaxLegalCorteCuatro;
		this.capitalSocialCorteUno = capitalSocialCorteUno;
		this.capitalSocialCorteDos = capitalSocialCorteDos;
		this.capitalSocialCorteTres = capitalSocialCorteTres;
		this.capitalSocialCorteCuatro = capitalSocialCorteCuatro;
		this.capitalMinRequeridoCorteUno = capitalMinRequeridoCorteUno;
		this.capitalMinRequeridoCorteDos = capitalMinRequeridoCorteDos;
		this.capitalMinRequeridoCorteTres = capitalMinRequeridoCorteTres;
		this.capitalMinRequeridoCorteCuatro = capitalMinRequeridoCorteCuatro;
	}

}
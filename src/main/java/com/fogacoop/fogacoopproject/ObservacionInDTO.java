package com.fogacoop.fogacoopproject;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class ObservacionInDTO implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String tipIdentificacion;
	private java.lang.String numIdentificacion;
	private java.lang.String fechaInicio;
	private java.lang.String cargo;
	private java.lang.String descripcion;
	private java.lang.String fecha;

	public ObservacionInDTO() {
	}

	public java.lang.String getTipIdentificacion() {
		return this.tipIdentificacion;
	}

	public void setTipIdentificacion(java.lang.String tipIdentificacion) {
		this.tipIdentificacion = tipIdentificacion;
	}

	public java.lang.String getNumIdentificacion() {
		return this.numIdentificacion;
	}

	public void setNumIdentificacion(java.lang.String numIdentificacion) {
		this.numIdentificacion = numIdentificacion;
	}

	public java.lang.String getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(java.lang.String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public java.lang.String getCargo() {
		return this.cargo;
	}

	public void setCargo(java.lang.String cargo) {
		this.cargo = cargo;
	}

	public java.lang.String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(java.lang.String descripcion) {
		this.descripcion = descripcion;
	}

	public java.lang.String getFecha() {
		return this.fecha;
	}

	public void setFecha(java.lang.String fecha) {
		this.fecha = fecha;
	}

	public ObservacionInDTO(java.lang.String tipIdentificacion,
			java.lang.String numIdentificacion, java.lang.String fechaInicio,
			java.lang.String cargo, java.lang.String descripcion,
			java.lang.String fecha) {
		this.tipIdentificacion = tipIdentificacion;
		this.numIdentificacion = numIdentificacion;
		this.fechaInicio = fechaInicio;
		this.cargo = cargo;
		this.descripcion = descripcion;
		this.fecha = fecha;
	}

}
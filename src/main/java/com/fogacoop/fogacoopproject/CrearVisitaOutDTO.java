package com.fogacoop.fogacoopproject;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class CrearVisitaOutDTO implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String idVisita;

	public CrearVisitaOutDTO() {
	}

	public java.lang.String getIdVisita() {
		return this.idVisita;
	}

	public void setIdVisita(java.lang.String idVisita) {
		this.idVisita = idVisita;
	}

	public CrearVisitaOutDTO(java.lang.String idVisita) {
		this.idVisita = idVisita;
	}

}
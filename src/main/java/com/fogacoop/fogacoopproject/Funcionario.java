package com.fogacoop.fogacoopproject;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class Funcionario implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String tipoIdentificacion;
	private java.lang.String numIdentificacion;
	private java.lang.String nombre;
	private java.lang.String login;
	private java.lang.String correo;
	private com.fogacoop.fogacoopproject.NameValue cargo;
	private com.fogacoop.fogacoopproject.NameValue area;

	private java.lang.String fecInicio;

	public Funcionario() {
	}

	public java.lang.String getTipoIdentificacion() {
		return this.tipoIdentificacion;
	}

	public void setTipoIdentificacion(java.lang.String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public java.lang.String getNumIdentificacion() {
		return this.numIdentificacion;
	}

	public void setNumIdentificacion(java.lang.String numIdentificacion) {
		this.numIdentificacion = numIdentificacion;
	}

	public java.lang.String getNombre() {
		return this.nombre;
	}

	public void setNombre(java.lang.String nombre) {
		this.nombre = nombre;
	}

	public java.lang.String getLogin() {
		return this.login;
	}

	public void setLogin(java.lang.String login) {
		this.login = login;
	}

	public java.lang.String getCorreo() {
		return this.correo;
	}

	public void setCorreo(java.lang.String correo) {
		this.correo = correo;
	}

	public com.fogacoop.fogacoopproject.NameValue getCargo() {
		return this.cargo;
	}

	public void setCargo(com.fogacoop.fogacoopproject.NameValue cargo) {
		this.cargo = cargo;
	}

	public com.fogacoop.fogacoopproject.NameValue getArea() {
		return this.area;
	}

	public void setArea(com.fogacoop.fogacoopproject.NameValue area) {
		this.area = area;
	}

	public java.lang.String getFecInicio() {
		return this.fecInicio;
	}

	public void setFecInicio(java.lang.String fecInicio) {
		this.fecInicio = fecInicio;
	}

	public Funcionario(java.lang.String tipoIdentificacion,
			java.lang.String numIdentificacion, java.lang.String nombre,
			java.lang.String login, java.lang.String correo,
			com.fogacoop.fogacoopproject.NameValue cargo,
			com.fogacoop.fogacoopproject.NameValue area,
			java.lang.String fecInicio) {
		this.tipoIdentificacion = tipoIdentificacion;
		this.numIdentificacion = numIdentificacion;
		this.nombre = nombre;
		this.login = login;
		this.correo = correo;
		this.cargo = cargo;
		this.area = area;
		this.fecInicio = fecInicio;
	}

}
package com.fogacoop.fogacoopproject;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class ErogacionesAYC implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String tipoEntidad;
	private java.lang.String codigoEntidad;
	private java.lang.String unidadCaptura;
	private java.lang.String renglon;
	private java.lang.String tipoIdent;
	private java.lang.String nroIdent;
	private java.lang.String valor;
	private java.lang.String concepto;
	private java.lang.String descripcionOtro;
	private java.lang.String fechaCorte;
	private java.lang.String idFormato;
	public java.lang.String getTipoEntidad() {
		return tipoEntidad;
	}
	public void setTipoEntidad(java.lang.String tipoEntidad) {
		this.tipoEntidad = tipoEntidad;
	}
	public java.lang.String getCodigoEntidad() {
		return codigoEntidad;
	}
	public void setCodigoEntidad(java.lang.String codigoEntidad) {
		this.codigoEntidad = codigoEntidad;
	}
	public java.lang.String getUnidadCaptura() {
		return unidadCaptura;
	}
	public void setUnidadCaptura(java.lang.String unidadCaptura) {
		this.unidadCaptura = unidadCaptura;
	}
	public java.lang.String getRenglon() {
		return renglon;
	}
	public void setRenglon(java.lang.String renglon) {
		this.renglon = renglon;
	}
	public java.lang.String getTipoIdent() {
		return tipoIdent;
	}
	public void setTipoIdent(java.lang.String tipoIdent) {
		this.tipoIdent = tipoIdent;
	}
	public java.lang.String getNroIdent() {
		return nroIdent;
	}
	public void setNroIdent(java.lang.String nroIdent) {
		this.nroIdent = nroIdent;
	}
	public java.lang.String getValor() {
		return valor;
	}
	public void setValor(java.lang.String valor) {
		this.valor = valor;
	}
	public java.lang.String getConcepto() {
		return concepto;
	}
	public void setConcepto(java.lang.String concepto) {
		this.concepto = concepto;
	}
	public java.lang.String getDescripcionOtro() {
		return descripcionOtro;
	}
	public void setDescripcionOtro(java.lang.String descripcionOtro) {
		this.descripcionOtro = descripcionOtro;
	}
	public java.lang.String getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(java.lang.String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	public java.lang.String getIdFormato() {
		return this.idFormato;
	}
	public void setIdFormato(java.lang.String idFormato) {
		this.idFormato = idFormato;
	}
	public ErogacionesAYC() {
	}
	public ErogacionesAYC(java.lang.String tipoEntidad,
			java.lang.String codigoEntidad, java.lang.String unidadCaptura,
			java.lang.String renglon, java.lang.String tipoIdent,
			java.lang.String nroIdent, java.lang.String valor,
			java.lang.String concepto, java.lang.String descripcionOtro,
			java.lang.String fechaCorte, java.lang.String idFormato) {
		this.tipoEntidad = tipoEntidad;
		this.codigoEntidad = codigoEntidad;
		this.unidadCaptura = unidadCaptura;
		this.renglon = renglon;
		this.tipoIdent = tipoIdent;
		this.nroIdent = nroIdent;
		this.valor = valor;
		this.concepto = concepto;
		this.descripcionOtro = descripcionOtro;
		this.fechaCorte = fechaCorte;
		this.idFormato = idFormato;
	}

}
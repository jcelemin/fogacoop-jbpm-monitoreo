package com.fogacoop.fogacoopproject;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class InsertarRegCoopInDTO implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String fechaCorte;

	public InsertarRegCoopInDTO() {
	}

	public java.lang.String getFechaCorte() {
		return this.fechaCorte;
	}

	public void setFechaCorte(java.lang.String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public InsertarRegCoopInDTO(java.lang.String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

}